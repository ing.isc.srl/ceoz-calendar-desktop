module.exports = {
  client: {
    service: {
      endpoint: {
        skipSSLValidation: true,
      },
      name: 'ceoz-desktop',
    },
    // Files processed by the extension
    includes: ['src/**/*.jsx', 'src/**/*.js'],
  },
};
